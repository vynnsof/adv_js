class Hamburger {

    constructor(size, stuffing) {
        try {
            if (!size || !stuffing) {
                throw new HamburgerException('one of properties are missing')
            }
            if (size.id !== 'small' && size.id !== 'large') {
                throw new HamburgerException('Please enter correct size')
            }
            if (stuffing.id !== 'salad' && stuffing.id !== 'cheese' && stuffing.id !== 'potato') {
                throw new HamburgerException('Please enter correct stuffing')
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.toppings = [];
            }
        } catch (e) {
            console.log(e.message)
        }
    };

    addTopping = function (topping) {
        try {
            if (!topping) {
                throw new HamburgerException('not added stuffing')
            } else if (this.toppings.includes(topping)) {
                throw new HamburgerException('topping already exists')
            } else {
                this.toppings.push(topping);
            }
        } catch (e) {
            console.log(e.message)
        }
    };

    removeTopping = function (topping) {
        try {
            if (!this.toppings.includes(topping)) {
                throw new HamburgerException('topping is not found')
            } else if (this.toppings.includes(topping)) {
                this.toppings.splice(this.toppings.indexOf(topping), 1);
            }
        } catch (e) {
            console.log(e.message)
        }
    };

    calculatePrice = function () {
        return Object.values(this).filter(el => typeof el !== "function").reduce((totalPrice, arrayObj) => {
            if (Array.isArray(arrayObj)) {
                arrayObj.forEach(function (obj) {
                    totalPrice += obj.price;
                });
                return totalPrice
            }
            totalPrice += arrayObj.price;
            return totalPrice
        }, 0);
    };

    calculateCalories = function () {
        return Object.values(this).filter(el => typeof el !== "function").reduce((totalCalories, arrayObj) => {
            if (Array.isArray(arrayObj)) {
                arrayObj.forEach(function (obj) {
                    totalCalories += obj.calories;
                });
                return totalCalories
            }
            totalCalories += arrayObj.calories;
            return totalCalories
        }, 0);
    };

    getToppings = function () {
        return this.toppings;
    };

    getSize = function () {
        return this.size;
    };


    getStuffing = function () {
        return this.stuffing;
    };
};

function HamburgerException(message) {
    this.message = message;
};

Hamburger.SIZE_SMALL = {id: 'small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {id: 'large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {id: 'cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {id: 'salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {id: 'potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {id: 'mayo', price: 15, calories: 0};
Hamburger.TOPPING_SPICE = {id: 'spice', price: 20, calories: 5};

const hamburgerOrdered = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
hamburgerOrdered.addTopping(Hamburger.TOPPING_MAYO);
hamburgerOrdered.addTopping(Hamburger.TOPPING_SPICE);

console.log(hamburgerOrdered);
console.log(`Розмір:  ${hamburgerOrdered.size.id}`);
console.log(`К-сть калорій: ${hamburgerOrdered.calculateCalories()}`);
console.log(`Ціна: ${hamburgerOrdered.calculatePrice()}`);